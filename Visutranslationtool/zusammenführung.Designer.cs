﻿namespace VisuTranslationTool
{
    partial class zusammenführung
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOpenSiemensData = new System.Windows.Forms.Button();
            this.buttonOpenTranslateData = new System.Windows.Forms.Button();
            this.comboBoxSiemensGrundspalte = new System.Windows.Forms.ComboBox();
            this.comboBoxÜbersetzungGrundspalte = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxÜbersetzungNeueSprachenspalte = new System.Windows.Forms.ComboBox();
            this.comboBoxSiemensNeueSprachenspalte = new System.Windows.Forms.ComboBox();
            this.buttonAbbrechen = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.labelSiemensDataName = new System.Windows.Forms.Label();
            this.labelÜbersetzungDataName = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOpenSiemensData
            // 
            this.buttonOpenSiemensData.Location = new System.Drawing.Point(6, 6);
            this.buttonOpenSiemensData.Name = "buttonOpenSiemensData";
            this.buttonOpenSiemensData.Size = new System.Drawing.Size(215, 23);
            this.buttonOpenSiemensData.TabIndex = 0;
            this.buttonOpenSiemensData.Text = "Öffne Siemensübersetzungsdatei";
            this.buttonOpenSiemensData.UseVisualStyleBackColor = true;
            this.buttonOpenSiemensData.Click += new System.EventHandler(this.buttonOpenSiemensData_Click);
            // 
            // buttonOpenTranslateData
            // 
            this.buttonOpenTranslateData.Location = new System.Drawing.Point(6, 6);
            this.buttonOpenTranslateData.Name = "buttonOpenTranslateData";
            this.buttonOpenTranslateData.Size = new System.Drawing.Size(215, 23);
            this.buttonOpenTranslateData.TabIndex = 1;
            this.buttonOpenTranslateData.Text = "Öffne Übersetzungsdatei";
            this.buttonOpenTranslateData.UseVisualStyleBackColor = true;
            this.buttonOpenTranslateData.Click += new System.EventHandler(this.buttonOpenTranslateData_Click);
            // 
            // comboBoxSiemensGrundspalte
            // 
            this.comboBoxSiemensGrundspalte.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSiemensGrundspalte.Enabled = false;
            this.comboBoxSiemensGrundspalte.FormattingEnabled = true;
            this.comboBoxSiemensGrundspalte.Location = new System.Drawing.Point(6, 76);
            this.comboBoxSiemensGrundspalte.Name = "comboBoxSiemensGrundspalte";
            this.comboBoxSiemensGrundspalte.Size = new System.Drawing.Size(215, 21);
            this.comboBoxSiemensGrundspalte.TabIndex = 2;
            // 
            // comboBoxÜbersetzungGrundspalte
            // 
            this.comboBoxÜbersetzungGrundspalte.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxÜbersetzungGrundspalte.Enabled = false;
            this.comboBoxÜbersetzungGrundspalte.FormattingEnabled = true;
            this.comboBoxÜbersetzungGrundspalte.Location = new System.Drawing.Point(6, 76);
            this.comboBoxÜbersetzungGrundspalte.Name = "comboBoxÜbersetzungGrundspalte";
            this.comboBoxÜbersetzungGrundspalte.Size = new System.Drawing.Size(215, 21);
            this.comboBoxÜbersetzungGrundspalte.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Location = new System.Drawing.Point(6, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Grundsprachenspalte:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Enabled = false;
            this.label2.Location = new System.Drawing.Point(6, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Grundsprachenspalte:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Enabled = false;
            this.label3.Location = new System.Drawing.Point(6, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Einzufügende Sprachspalte:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Enabled = false;
            this.label4.Location = new System.Drawing.Point(6, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(167, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Neue Sprache einfügen in Spalte:";
            // 
            // comboBoxÜbersetzungNeueSprachenspalte
            // 
            this.comboBoxÜbersetzungNeueSprachenspalte.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxÜbersetzungNeueSprachenspalte.Enabled = false;
            this.comboBoxÜbersetzungNeueSprachenspalte.FormattingEnabled = true;
            this.comboBoxÜbersetzungNeueSprachenspalte.Location = new System.Drawing.Point(6, 123);
            this.comboBoxÜbersetzungNeueSprachenspalte.Name = "comboBoxÜbersetzungNeueSprachenspalte";
            this.comboBoxÜbersetzungNeueSprachenspalte.Size = new System.Drawing.Size(215, 21);
            this.comboBoxÜbersetzungNeueSprachenspalte.TabIndex = 7;
            // 
            // comboBoxSiemensNeueSprachenspalte
            // 
            this.comboBoxSiemensNeueSprachenspalte.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSiemensNeueSprachenspalte.Enabled = false;
            this.comboBoxSiemensNeueSprachenspalte.FormattingEnabled = true;
            this.comboBoxSiemensNeueSprachenspalte.Location = new System.Drawing.Point(6, 123);
            this.comboBoxSiemensNeueSprachenspalte.Name = "comboBoxSiemensNeueSprachenspalte";
            this.comboBoxSiemensNeueSprachenspalte.Size = new System.Drawing.Size(215, 21);
            this.comboBoxSiemensNeueSprachenspalte.TabIndex = 6;
            // 
            // buttonAbbrechen
            // 
            this.buttonAbbrechen.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonAbbrechen.Location = new System.Drawing.Point(384, 4);
            this.buttonAbbrechen.Name = "buttonAbbrechen";
            this.buttonAbbrechen.Size = new System.Drawing.Size(75, 23);
            this.buttonAbbrechen.TabIndex = 10;
            this.buttonAbbrechen.Text = "Abbrechen";
            this.buttonAbbrechen.UseVisualStyleBackColor = true;
            // 
            // buttonOK
            // 
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(303, 4);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 11;
            this.buttonOK.Text = "Ok";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // labelSiemensDataName
            // 
            this.labelSiemensDataName.Location = new System.Drawing.Point(6, 29);
            this.labelSiemensDataName.Name = "labelSiemensDataName";
            this.labelSiemensDataName.Size = new System.Drawing.Size(215, 13);
            this.labelSiemensDataName.TabIndex = 12;
            this.labelSiemensDataName.Text = "Keine Datei gewählt";
            this.labelSiemensDataName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelÜbersetzungDataName
            // 
            this.labelÜbersetzungDataName.Location = new System.Drawing.Point(6, 29);
            this.labelÜbersetzungDataName.Name = "labelÜbersetzungDataName";
            this.labelÜbersetzungDataName.Size = new System.Drawing.Size(215, 13);
            this.labelÜbersetzungDataName.TabIndex = 13;
            this.labelÜbersetzungDataName.Text = "Keine Datei gewählt";
            this.labelÜbersetzungDataName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel1.Controls.Add(this.comboBoxSiemensNeueSprachenspalte);
            this.panel1.Controls.Add(this.buttonOpenSiemensData);
            this.panel1.Controls.Add(this.labelSiemensDataName);
            this.panel1.Controls.Add(this.comboBoxSiemensGrundspalte);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(9, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(227, 151);
            this.panel1.TabIndex = 14;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel2.Controls.Add(this.buttonOpenTranslateData);
            this.panel2.Controls.Add(this.comboBoxÜbersetzungGrundspalte);
            this.panel2.Controls.Add(this.labelÜbersetzungDataName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.comboBoxÜbersetzungNeueSprachenspalte);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(244, 8);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(227, 151);
            this.panel2.TabIndex = 15;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel3.Controls.Add(this.buttonOK);
            this.panel3.Controls.Add(this.buttonAbbrechen);
            this.panel3.Location = new System.Drawing.Point(9, 165);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(462, 30);
            this.panel3.TabIndex = 16;
            // 
            // zusammenführung
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 202);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "zusammenführung";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Zusammenführung - Siemensdatei mit der Übersetzungsdatei";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonOpenSiemensData;
        private System.Windows.Forms.Button buttonOpenTranslateData;
        private System.Windows.Forms.ComboBox comboBoxSiemensGrundspalte;
        private System.Windows.Forms.ComboBox comboBoxÜbersetzungGrundspalte;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxÜbersetzungNeueSprachenspalte;
        private System.Windows.Forms.ComboBox comboBoxSiemensNeueSprachenspalte;
        private System.Windows.Forms.Button buttonAbbrechen;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Label labelSiemensDataName;
        private System.Windows.Forms.Label labelÜbersetzungDataName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
    }
}