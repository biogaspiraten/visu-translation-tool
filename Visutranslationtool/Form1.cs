﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Data.OleDb;

namespace VisuTranslationTool
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonDateiÖffnen_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "xlsx files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            ofd.FilterIndex = 1;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                //Bereitet die Connection vor
                string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + ofd.FileName + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";
                OleDbConnection objConn = new OleDbConnection(connectionString);
                objConn.Open();

                //Ermittelt die Namen der Arbeitsmappen
                DataTable dtExcelSchema = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string[] strArray = new string[dtExcelSchema.Rows.Count];
                int iCounter = 0;
                foreach (DataRow item in dtExcelSchema.Rows)
                {
                    strArray[iCounter] = item.ItemArray[2].ToString();
                    iCounter++;
                }

                //Öffnet ein Fenster zum auswählen einer Arbeitsmappe
                SheetChoice frm = new SheetChoice(strArray);
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    //Setzt das DataGridView zurück
                    dataGridView1.DataSource = null;

                    //Ließt die Daten aus der Exceldatei
                    OleDbCommand ObjCommand = new OleDbCommand("SELECT * FROM [" + frm.Sheet + "]", objConn);
                    OleDbDataAdapter objAdp = new OleDbDataAdapter();
                    objAdp.SelectCommand = ObjCommand;

                    //Füllt das DataGridVIew
                    DataSet myDataSet = new DataSet();
                    objAdp.Fill(myDataSet);
                    dataGridView1.DataSource = myDataSet.Tables["Table"];
                    objConn.Close();

                    //Versorgt das Interface mit den aktuellen Tabelleninfos
                    comboBox1.Items.Clear();
                    comboBox2.Items.Clear();
                    comboBox3.Items.Clear();
                    comboBox4.Items.Clear();
                    comboBox5.Items.Clear();
                    foreach (DataGridViewColumn item in dataGridView1.Columns)
                    {
                        comboBox1.Items.Add(item.HeaderText);
                        comboBox2.Items.Add(item.HeaderText);
                        comboBox3.Items.Add(item.HeaderText);
                        comboBox4.Items.Add(item.HeaderText);
                        comboBox5.Items.Add(item.HeaderText);
                    }
                }
            }
            labelEinträge.Text = dataGridView1.Rows.Count.ToString() + " Einträge";
            Cursor.Current = Cursors.Default;
        }

        private void buttonDateiSpeichern_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "xlsx files (*.xlsx)|*.xlsx";
            sfd.FilterIndex = 1;
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                Cursor.Current = Cursors.WaitCursor;

                ExcelFile excelfile = new ExcelFile();

                excelfile.SetSheetname(1, "TextList");

                int iCounter = 1;
                foreach (DataGridViewColumn column in dataGridView1.Columns)
                {
                    excelfile.AddHeader(column.HeaderText, iCounter, 1, 0, 0);

                    iCounter++;
                }

                object[,] data;
                data = new string[dataGridView1.Rows.Count, dataGridView1.Columns.Count];

                iCounter = 1;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    int iCounter1 = 0;
                    foreach (DataGridViewColumn column in dataGridView1.Columns)
                    {
                        if (row.Cells[iCounter1].Value != null)
                        {
                            data[iCounter - 1, iCounter1] = row.Cells[iCounter1].Value.ToString();
                        }
                        else
                        {
                            data[iCounter - 1, iCounter1] = "";
                        }

                        iCounter1++;
                    }

                    iCounter++;
                }

                excelfile.FillExcelWithData(1, 2, data, false);
                excelfile.SaveExcel(sfd.FileName);

                Cursor.Current = Cursors.Default;
            }
        }

        private void buttonLeeren_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            dataGridView1.DataSource = null;

            labelEinträge.Text = dataGridView1.Rows.Count.ToString() + " Einträge";

            Cursor.Current = Cursors.Default;
        }

        private void buttonLöschen2_Click(object sender, EventArgs e)
        {
            if (numericUpDown1.Value > numericUpDown2.Value)
            {
                MessageBox.Show("Der Anfangswert muss kleiner gleiche der Endwert sein!");
                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            for (int i = Convert.ToInt32(numericUpDown2.Value); i > Convert.ToInt32(numericUpDown1.Value) - 1; i--)
            {
                try
                {
                    dataGridView1.Rows.RemoveAt(i);
                }
                catch (Exception)
                {

                    throw;
                }
            }

            labelEinträge.Text = dataGridView1.Rows.Count.ToString() + " Einträge";

            Cursor.Current = Cursors.Default;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (comboBox4.Text == "")
            {
                MessageBox.Show("Es muss eine Reihe ausgewählt werden!");
                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            int iIndex = 0;

            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                if (column.HeaderText == comboBox4.Text)
                {
                    iIndex = column.Index;
                    break;
                }
            }

            //Liste sortieren, damit man die doppelten Einträge löschen kann.
            try
            {
                ListSortDirection sortDirection = ListSortDirection.Ascending;
                dataGridView1.Sort(dataGridView1.Columns[iIndex], sortDirection);
            }
            catch (Exception)
            {

            }



            //Doppelte Einträge erkennen und den Index merken
            string strLast = null;
            List<int> listInt = new List<int>();
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (row.Cells[iIndex].Value != null)
                {
                    if (strLast == row.Cells[iIndex].Value.ToString())
                    {
                        listInt.Add(row.Index);
                    }
                    strLast = row.Cells[iIndex].Value.ToString();
                }
            }

            //Liste der Indexe der zu löschenden Dateien sortieren
            listInt.Sort();

            //Einträge löschen
            for (int i = listInt.Count; i > -1; i--)
            {
                try
                {
                    DataGridViewRow row = dataGridView1.Rows[listInt[i]];
                    dataGridView1.Rows.Remove(row);
                }
                catch (Exception)
                {

                }
            }

            labelEinträge.Text = dataGridView1.Rows.Count.ToString() + " Einträge";

            Cursor.Current = Cursors.Default;
        }

        private void buttonLöschen1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "")
            {
                MessageBox.Show("Es muss eine Reihe ausgewählt werden!");
                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            int iIndex = 0;

            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                if (column.HeaderText == comboBox1.Text)
                {
                    iIndex = column.Index;
                    break;
                }
            }

            for (int i = dataGridView1.Rows.Count - 1; i > -1; i--)
            {
                try
                {
                    if ((string)dataGridView1.Rows[i].Cells[iIndex].Value == textBoxSuE.Text)
                    {
                        dataGridView1.Rows.Remove(dataGridView1.Rows[i]);
                    }
                }
                catch (Exception)
                {

                }
            }

            labelEinträge.Text = dataGridView1.Rows.Count.ToString() + " Einträge";

            Cursor.Current = Cursors.Default;
        }

        private void buttonLöschen3_Click(object sender, EventArgs e)
        {
            if (comboBox2.Text == "")
            {
                MessageBox.Show("Es muss eine Reihe ausgewählt werden!");
                return;
            }
            
            Cursor.Current = Cursors.WaitCursor;

            int iIndex = 0;

            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                if (column.HeaderText == comboBox2.Text)
                {
                    iIndex = column.Index;
                    break;
                }
            }

            List<string> listZeichen = new List<string>();
            for (int i = 65; i < 90; i++)
            {
                listZeichen.Add(Convert.ToChar(i).ToString());
            }
            for (int i = 97; i < 122; i++)
            {
                listZeichen.Add(Convert.ToChar(i).ToString());
            }

            for (int i1 = dataGridView1.Rows.Count; i1 > -1; i1--)
            {
                try
                {
                    DataGridViewRow row = dataGridView1.Rows[i1];

                    int iCounter = 0;
                    string strValue = row.Cells[iIndex].Value.ToString();

                    for (int i = 0; i < strValue.Length; i++)
                    {
                        foreach (string zeichen in listZeichen)
                        {
                            if (strValue.Substring(i, 1) == zeichen)
                            {
                                iCounter++;
                                break;
                            }
                        }
                    }

                    if (iCounter <= Convert.ToInt32(numericUpDownBuchstaben.Value))
                    {
                        dataGridView1.Rows.Remove(row);
                    }
                }
                catch (Exception)
                {

                }
            }

            labelEinträge.Text = dataGridView1.Rows.Count.ToString() + " Einträge";

            Cursor.Current = Cursors.Default;
        }

        private void buttonLöschen4_Click(object sender, EventArgs e)
        {
            if (comboBox3.Text == "" && textBoxLöschen4.Text == "")
            {
                MessageBox.Show("Es muss eine Reihe ausgewählt werden und mindestens ein Buchstabe in dem Textfeld stehen!");
                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            int iIndex = 0;

            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                if (column.HeaderText == comboBox3.Text)
                {
                    iIndex = column.Index;
                    break;
                }
            }

            for (int i = dataGridView1.Rows.Count - 1; i > -1; i--)
            {
                try
                {
                    if (dataGridView1.Rows[i].Cells[iIndex].Value.ToString().Contains(textBoxLöschen4.Text))
                    {
                        dataGridView1.Rows.Remove(dataGridView1.Rows[i]);
                    }
                }
                catch (Exception)
                {

                }
            }

            labelEinträge.Text = dataGridView1.Rows.Count.ToString() + " Einträge";

            Cursor.Current = Cursors.Default;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (comboBox5.Text == "")
            {
                MessageBox.Show("Es muss eine Reihe ausgewählt werden!");
                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            int iIndex = 0;

            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                if (column.HeaderText == comboBox5.Text)
                {
                    iIndex = column.Index;
                    break;
                }
            }

            for (int i = dataGridView1.Rows.Count - 1; i > -1; i--)
            {
                try
                {
                    bool bCheck = true;
                    string strValue = dataGridView1.Rows[i].Cells[iIndex].Value.ToString();

                    foreach (char zeichen in textBoxZeichen.Text.ToCharArray())
                    {
                        if (!strValue.Contains(zeichen))
                        {
                            bCheck = false;
                        }
                    }
                    if (bCheck)
                    {
                        dataGridView1.Rows.Remove(dataGridView1.Rows[i]);
                    }
                }
                catch (Exception)
                {

                }
            }

            labelEinträge.Text = dataGridView1.Rows.Count.ToString() + " Einträge";

            Cursor.Current = Cursors.Default;
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            zeichenaussortierer frm = new zeichenaussortierer(textBoxZeichen.Text);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                textBoxZeichen.Text = frm.Selection;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            zusammenführung frm = new zusammenführung();
            frm.ShowDialog();
        }

        private void labelEinträge_Click(object sender, EventArgs e)
        {
            labelEinträge.Text = dataGridView1.Rows.Count.ToString() + " Einträge";
        }
    }
}