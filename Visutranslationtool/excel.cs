﻿using System;
using System.Drawing;
using System.Reflection;
using System.Runtime.InteropServices;

namespace VisuTranslationTool
{
    public class ExcelFile
    {
        private Microsoft.Office.Interop.Excel.Application _excelApplication = null;
        private Microsoft.Office.Interop.Excel.Workbooks _workBooks = null;
        private Microsoft.Office.Interop.Excel._Workbook _workBook = null;
        private object _value = Missing.Value;
        private Microsoft.Office.Interop.Excel.Sheets _excelSheets = null;
        private Microsoft.Office.Interop.Excel._Worksheet _excelSheet = null;
        private Microsoft.Office.Interop.Excel.Range _excelRange = null;
        private Microsoft.Office.Interop.Excel._Worksheet _excelSheet2 = null;

        public ExcelFile()
        {
            ActivateExcel();
        }

        public void AddHeader(string text, int columnnumber, int rownumber, int colspan, int rowspan, bool autofit = true)
        {
            int endcolumn = columnnumber;
            if (colspan > 0)
                endcolumn += colspan - 1;

            string startcolumnname = GetExcelColumnName(columnnumber) + rownumber;

            if (rowspan > 0)
                rownumber = rownumber + rowspan - 1;

            string endcolumnname = GetExcelColumnName(endcolumn) + rownumber;

            _excelRange = _excelSheet.get_Range(startcolumnname, endcolumnname);

            if (rowspan > 0 || colspan > 0)
                _excelRange.Merge(_value);

            _excelRange.set_Value(_value, text);

            if (autofit)
                _excelRange.EntireColumn.AutoFit();
        }

        public void FillExcelWithData(int startcolumn, int startrow, object[,] data, bool right, bool autofit = true)
        {
            int rowcnt = data.GetLength(0);
            int columncnt = data.GetLength(1);

            string columnname = GetExcelColumnName(startcolumn);

            _excelRange = _excelSheet.get_Range(columnname + startrow, _value);
            _excelRange = _excelRange.get_Resize(rowcnt, columncnt);
            _excelRange.set_Value(Missing.Value, data);
            if (right)
                _excelRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            if (autofit)
                _excelRange.EntireColumn.AutoFit();
        }

        public void ColumnColor(int columnnr, Color color, int fromRow, int toRow)
        {
            string columnname = GetExcelColumnName(columnnr);
            string startcolumnname = columnname + fromRow;
            string endcolumnname = columnname + toRow;

            _excelRange = _excelSheet.get_Range(startcolumnname, endcolumnname);
            _excelRange.Interior.Color = color;
        }

        public void RowColor(int Rownr, Color color, Color bordercolor, int fromColumn, int toColumn)
        {
            string fromcolumnname = GetExcelColumnName(fromColumn);
            string tocolumnname = GetExcelColumnName(toColumn);

            string startcolumnname = fromcolumnname + Rownr;
            string endcolumnname = tocolumnname + Rownr;

            _excelRange = _excelSheet.get_Range(startcolumnname, endcolumnname);
            _excelRange.Interior.Color = color;
            _excelRange.Borders.Color = bordercolor;
        }

        public void FreezeFirstColumns(int count)
        {
            _excelSheet.Activate();
            _excelSheet.Application.ActiveWindow.SplitColumn = count;
            _excelSheet.Application.ActiveWindow.FreezePanes = true;
        }

        public void FreezeFirstRows(int count)
        {
            _excelSheet.Activate();
            _excelSheet.Application.ActiveWindow.SplitRow = count;
            _excelSheet.Application.ActiveWindow.FreezePanes = true;
        }

        /// <summary>
        /// save the excel sheet to the location with file name
        /// </summary>
        public void SaveExcel(string fileName)
        {
            _excelApplication.DisplayAlerts = false;
            _excelApplication.AlertBeforeOverwriting = false;

            _workBook.SaveCopyAs(fileName);
            //_workBook.SaveAs(fileName, _value, _value,
            //    _value, _value, _value, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
            //    _value, _value, _value, _value, null);

            _workBook.Close(false, _value, _value);
            _excelApplication.Quit();

            GC.Collect();
            GC.WaitForPendingFinalizers();

            if (_excelRange != null)
                Marshal.FinalReleaseComObject(_excelRange);
            if (_excelSheet != null)
                Marshal.FinalReleaseComObject(_excelSheet);
            if (_excelSheet2 != null)
                Marshal.FinalReleaseComObject(_excelSheet2);
            if (_excelSheets != null)
                Marshal.FinalReleaseComObject(_excelSheets);
            if (_workBook != null)
                Marshal.FinalReleaseComObject(_workBook);
            if (_workBooks != null)
                Marshal.FinalReleaseComObject(_workBooks);
            if (_excelApplication != null)
                Marshal.FinalReleaseComObject(_excelApplication);
        }

        /// <summary>
        /// activate the excel application
        /// </summary>
        private void ActivateExcel()
        {
            _excelApplication = new Microsoft.Office.Interop.Excel.Application();
            _workBooks = (Microsoft.Office.Interop.Excel.Workbooks)_excelApplication.Workbooks;
            _workBook = (Microsoft.Office.Interop.Excel._Workbook)(_workBooks.Add(_value));
            _excelSheets = (Microsoft.Office.Interop.Excel.Sheets)_workBook.Worksheets;
            _excelSheet = (Microsoft.Office.Interop.Excel._Worksheet)(_excelSheets.get_Item(1));
        }

        public void SetSheet(int nr)
        {
            _excelSheet = (Microsoft.Office.Interop.Excel._Worksheet)(_excelSheets.get_Item(nr));
            _excelSheet.Activate();
        }

        public void SetSheetname(int nr, string name)
        {
            _excelSheet2 = (Microsoft.Office.Interop.Excel._Worksheet)(_excelSheets.get_Item(nr));
            _excelSheet2.Name = name;
        }

        /// <summary>
        /// Convert Columnnumber to Excel Column Name
        /// </summary>
        private string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }

        public void Show()
        {
            _excelApplication.Visible = true;
        }

        private void ReleaseObj(Object obj)
        {
            if (obj != null)
            {
                while (Marshal.ReleaseComObject(obj) > 0) { };
                obj = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
    }
}
