﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace VisuTranslationTool
{
    public partial class zusammenführung : Form
    {
        DataTable m_dtSiemens = new DataTable();
        DataTable m_dtÜbersetzungs = new DataTable();

        public zusammenführung()
        {
            InitializeComponent();
        }

        private void buttonOpenSiemensData_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            m_dtSiemens.Clear();

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "xlsx files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            ofd.FilterIndex = 1;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                //Bereitet die Connection vor
                string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + ofd.FileName + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";
                OleDbConnection objConn = new OleDbConnection(connectionString);
                objConn.Open();

                //Ermittelt die Namen der Arbeitsmappen
                DataTable dtExcelSchema = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string[] strArray = new string[dtExcelSchema.Rows.Count];
                int iCounter = 0;
                foreach (DataRow item in dtExcelSchema.Rows)
                {
                    strArray[iCounter] = item.ItemArray[2].ToString();
                    iCounter++;
                }

                //Öffnet ein Fenster zum auswählen einer Arbeitsmappe
                SheetChoice frm = new SheetChoice(strArray);
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    //Ließt die Daten aus der Exceldatei
                    OleDbCommand ObjCommand = new OleDbCommand("SELECT * FROM [" + frm.Sheet + "]", objConn);
                    OleDbDataAdapter objAdp = new OleDbDataAdapter();
                    objAdp.SelectCommand = ObjCommand;

                    //Füllt das DataGridVIew
                    objAdp.Fill(m_dtSiemens);

                    comboBoxSiemensGrundspalte.Items.Clear();
                    comboBoxSiemensNeueSprachenspalte.Items.Clear();
                    foreach (DataColumn item in m_dtSiemens.Columns)
                    {
                        comboBoxSiemensGrundspalte.Items.Add(item.Caption);
                        comboBoxSiemensNeueSprachenspalte.Items.Add(item.Caption);
                    }
                    objConn.Close();

                    comboBoxSiemensGrundspalte.Enabled = true;
                    comboBoxSiemensNeueSprachenspalte.Enabled = true;
                    label1.Enabled = true;
                    label4.Enabled = true;
                }
            }
            labelSiemensDataName.Text = ofd.SafeFileName;
            Cursor.Current = Cursors.Default;
        }

        private void buttonOpenTranslateData_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            m_dtÜbersetzungs.Clear();

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "xlsx files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            ofd.FilterIndex = 1;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                //Bereitet die Connection vor
                string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + ofd.FileName + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";
                OleDbConnection objConn = new OleDbConnection(connectionString);
                objConn.Open();

                //Ermittelt die Namen der Arbeitsmappen
                DataTable dtExcelSchema = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string[] strArray = new string[dtExcelSchema.Rows.Count];
                int iCounter = 0;
                foreach (DataRow item in dtExcelSchema.Rows)
                {
                    strArray[iCounter] = item.ItemArray[2].ToString();
                    iCounter++;
                }

                //Öffnet ein Fenster zum auswählen einer Arbeitsmappe
                SheetChoice frm = new SheetChoice(strArray);
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    //Ließt die Daten aus der Exceldatei
                    OleDbCommand ObjCommand = new OleDbCommand("SELECT * FROM [" + frm.Sheet + "]", objConn);
                    OleDbDataAdapter objAdp = new OleDbDataAdapter();
                    objAdp.SelectCommand = ObjCommand;

                    //Füllt das DataGridVIew
                    objAdp.Fill(m_dtÜbersetzungs);

                    comboBoxÜbersetzungGrundspalte.Items.Clear();
                    comboBoxÜbersetzungNeueSprachenspalte.Items.Clear();
                    foreach (DataColumn item in m_dtÜbersetzungs.Columns)
                    {
                        comboBoxÜbersetzungGrundspalte.Items.Add(item.Caption);
                        comboBoxÜbersetzungNeueSprachenspalte.Items.Add(item.Caption);
                    }
                    objConn.Close();

                    comboBoxÜbersetzungGrundspalte.Enabled = true;
                    comboBoxÜbersetzungNeueSprachenspalte.Enabled = true;
                    label2.Enabled = true;
                    label3.Enabled = true;
                }
            }
            labelÜbersetzungDataName.Text = ofd.SafeFileName;
            Cursor.Current = Cursors.Default;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.FileName = "SiemensSprachen";
            sfd.Filter = "xlsx files (*.xlsx)|*.xlsx";
            sfd.FilterIndex = 1;
           
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                ParallelCompareAndTakeOverTranslation();

                //FÜr Testzwecke
                //CompareAndTakeOverTranslation();

                ExcelFile excelfile = new ExcelFile();

                excelfile.SetSheetname(1, "TextList");

                int iCounter = 1;
                foreach (DataColumn column in m_dtSiemens.Columns)
                {
                    excelfile.AddHeader(column.Caption, iCounter, 1, 0, 0);

                    iCounter++;
                }

                object[,] data;
                data = new string[m_dtSiemens.Rows.Count, m_dtSiemens.Columns.Count];

                iCounter = 1;
                foreach (DataRow row in m_dtSiemens.Rows)
                {
                    int iCounter1 = 0;
                    foreach (DataColumn column in m_dtSiemens.Columns)
                    {
                        if (row.ItemArray[iCounter1] != null)
                        {
                            data[iCounter - 1, iCounter1] = row.ItemArray[iCounter1].ToString();
                        }
                        else
                        {
                            data[iCounter - 1, iCounter1] = "";
                        }

                        iCounter1++;
                    }

                    iCounter++;
                }

                excelfile.FillExcelWithData(1, 2, data, false);
                excelfile.SaveExcel(sfd.FileName);

                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary>
        /// Vergleicht die auf Grund zweier Spalten und ersetzt eine zuvor ausgewählte Spalte mit der übersetzten Spalte
        /// Diese Funktion arbeiten in Singlethread - Mode
        /// Sie dient für Testzwecke
        /// </summary>
        private void CompareAndTakeOverTranslation()
        {
            Cursor.Current = Cursors.WaitCursor;
            foreach (DataRow row1 in m_dtSiemens.Rows)
            {
                foreach (DataRow row2 in m_dtÜbersetzungs.Rows)
                {
                    if (row1.ItemArray[comboBoxSiemensGrundspalte.SelectedIndex].ToString() == row2.ItemArray[comboBoxÜbersetzungGrundspalte.SelectedIndex].ToString())
                    {
                        row1.BeginEdit();
                        row1[m_dtSiemens.Columns[comboBoxSiemensNeueSprachenspalte.SelectedIndex].Caption] = row2.ItemArray[comboBoxÜbersetzungNeueSprachenspalte.SelectedIndex].ToString();
                        row1.EndEdit();
                    }
                }
            }
        }

        /// <summary>
        /// Vergleicht die auf Grund zweier Spalten und ersetzt eine zuvor ausgewählte Spalte mit der übersetzten Spalte
        /// Diese Funktion arbeiten in Multithread - Mode
        /// </summary>
        private void ParallelCompareAndTakeOverTranslation()
        {
            
            int vonBasisindex = comboBoxSiemensGrundspalte.SelectedIndex; ;
            int nachBasisindex = comboBoxÜbersetzungGrundspalte.SelectedIndex;
            int vorhandeneSpaltenIndex = comboBoxSiemensNeueSprachenspalte.SelectedIndex;
            int uebersetzungsSpaltenIndex = comboBoxÜbersetzungNeueSprachenspalte.SelectedIndex;

            Parallel.For(0, m_dtSiemens.Rows.Count, index =>
            {
                foreach (DataRow row2 in m_dtÜbersetzungs.Rows)
                {

                    if (m_dtSiemens.Rows[index].ItemArray[vonBasisindex].ToString() == row2.ItemArray[vonBasisindex].ToString())
                    {
                        m_dtSiemens.Rows[index].BeginEdit();
                        m_dtSiemens.Rows[index][m_dtSiemens.Columns[vorhandeneSpaltenIndex].Caption] = row2.ItemArray[uebersetzungsSpaltenIndex].ToString();
                        m_dtSiemens.Rows[index].EndEdit();
                    }
                }

            });
        }

        private string m_strSheet;
        public string Sheet
        {
            get { return m_strSheet; }
            set { m_strSheet = value; }
        }
    }
}
