﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VisuTranslationTool
{
    public partial class zeichenaussortierer : Form
    {
        public zeichenaussortierer(string strSelection)
        {
            InitializeComponent();
            m_strSelection = strSelection;
        }

        private void zeichenaussortierer_Load(object sender, EventArgs e)
        {
            //Fügt 255 Ascii-Zeichen in das dgv ein
            for (int i = 0; i < 255;)
			{
                dataGridViewAsciiZiechen.Rows.Add(ToAscii(i), ToAscii(i + 1), ToAscii(i + 2), ToAscii(i + 3), ToAscii(i + 4), ToAscii(i + 5), ToAscii(i + 6), ToAscii(i + 7), ToAscii(i + 8), ToAscii(i + 9), ToAscii(i + 10), ToAscii(i + 11), ToAscii(i + 12), ToAscii(i + 13), ToAscii(i + 14), ToAscii(i + 15), ToAscii(i + 16), ToAscii(i + 17), ToAscii(i + 18), ToAscii(i + 19));
                i += 20;
			}

            //Selectiert im dgv die cellen die den text des strings enthalten
            dataGridViewAsciiZiechen.ClearSelection();
            foreach (char zeichen in m_strSelection.ToCharArray())
            {
                foreach (DataGridViewRow row in dataGridViewAsciiZiechen.Rows)
                {
                    for (int i = 0; i < 20; i++)
                    {
                        if (zeichen.ToString() == row.Cells[i].Value.ToString())
                        {
                            row.Cells[i].Selected = true;
                        }
                    }
                }
            }
        }

        private void dataGridViewAsciiZiechen_KeyPress(object sender, KeyPressEventArgs e)
        {
            foreach (DataGridViewRow row in dataGridViewAsciiZiechen.Rows)
            {
                for (int i = 0; i < 20; i++)
                {
                    if (row.Cells[i].Value.ToString() == e.KeyChar.ToString())
                    {
                        if (row.Cells[i].Selected)
                        {
                            row.Cells[i].Selected = false;
                        }
                        else
                        {
                            row.Cells[i].Selected = true;
                        }
                    }
                }
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            m_strSelection = "";
            foreach (DataGridViewCell cell in dataGridViewAsciiZiechen.SelectedCells)
            {
                m_strSelection += cell.Value.ToString();
            }
        }


        private string ToAscii(int iAscii)
        {
            return Convert.ToChar(iAscii).ToString();
        }

        private string m_strSelection;
        public string Selection
        {
            get { return m_strSelection; }
            set { m_strSelection = value; }
        }
    }
}
