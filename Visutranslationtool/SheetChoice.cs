﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VisuTranslationTool
{
    public partial class SheetChoice : Form
    {
        string[] m_strArray;
        Form1 m_frm1;

        public SheetChoice(string[] strArray)
        {
            InitializeComponent();
            m_strArray = strArray;

            SetInterface();
        }

        private void SetInterface()
        {
            listBoxSheets.Items.Clear();

            foreach (string item in m_strArray)
            {
                listBoxSheets.Items.Add(item);
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            m_frm1 = new Form1();

            if (listBoxSheets.SelectedItems.Count == 1)
	        {
                m_strSheet = (string)listBoxSheets.SelectedItem;
	        }
            else
            {
                MessageBox.Show("Es darf nur eine Arbeitsmappe ausgewählt sein!");
            }
        }


        private string m_strSheet;
        public string Sheet
        {
            get { return m_strSheet; }
            set { m_strSheet = value; }
        }

        private void listBoxSheets_DoubleClick(object sender, EventArgs e)
        {
            if (listBoxSheets.SelectedItems.Count == 1)
            {
                m_strSheet = (string)listBoxSheets.SelectedItem;
                DialogResult = DialogResult.OK;
            }
        }
    }
}
