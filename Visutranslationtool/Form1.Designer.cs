﻿namespace VisuTranslationTool
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonDateiÖffnen1 = new System.Windows.Forms.Button();
            this.buttonDateiSpeichern1 = new System.Windows.Forms.Button();
            this.buttonLeeren1 = new System.Windows.Forms.Button();
            this.buttonLöschen4 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.textBoxLöschen4 = new System.Windows.Forms.TextBox();
            this.numericUpDownBuchstaben = new System.Windows.Forms.NumericUpDown();
            this.buttonLöschen3 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonLöschen2 = new System.Windows.Forms.Button();
            this.buttonLöschen1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBoxSuE = new System.Windows.Forms.TextBox();
            this.buttonLöschen5 = new System.Windows.Forms.Button();
            this.labelEinträge = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonLöschen6 = new System.Windows.Forms.Button();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxZeichen = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBuchstaben)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(334, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(930, 500);
            this.dataGridView1.TabIndex = 0;
            // 
            // buttonDateiÖffnen1
            // 
            this.buttonDateiÖffnen1.Location = new System.Drawing.Point(12, 12);
            this.buttonDateiÖffnen1.Name = "buttonDateiÖffnen1";
            this.buttonDateiÖffnen1.Size = new System.Drawing.Size(94, 23);
            this.buttonDateiÖffnen1.TabIndex = 1;
            this.buttonDateiÖffnen1.Text = "Datei öffnen";
            this.buttonDateiÖffnen1.UseVisualStyleBackColor = true;
            this.buttonDateiÖffnen1.Click += new System.EventHandler(this.buttonDateiÖffnen_Click);
            // 
            // buttonDateiSpeichern1
            // 
            this.buttonDateiSpeichern1.Location = new System.Drawing.Point(112, 12);
            this.buttonDateiSpeichern1.Name = "buttonDateiSpeichern1";
            this.buttonDateiSpeichern1.Size = new System.Drawing.Size(94, 23);
            this.buttonDateiSpeichern1.TabIndex = 2;
            this.buttonDateiSpeichern1.Text = "Datei speichern";
            this.buttonDateiSpeichern1.UseVisualStyleBackColor = true;
            this.buttonDateiSpeichern1.Click += new System.EventHandler(this.buttonDateiSpeichern_Click);
            // 
            // buttonLeeren1
            // 
            this.buttonLeeren1.Location = new System.Drawing.Point(12, 41);
            this.buttonLeeren1.Name = "buttonLeeren1";
            this.buttonLeeren1.Size = new System.Drawing.Size(306, 23);
            this.buttonLeeren1.TabIndex = 10;
            this.buttonLeeren1.Text = "Tabelle leeren";
            this.buttonLeeren1.UseVisualStyleBackColor = true;
            this.buttonLeeren1.Click += new System.EventHandler(this.buttonLeeren_Click);
            // 
            // buttonLöschen4
            // 
            this.buttonLöschen4.Location = new System.Drawing.Point(262, 226);
            this.buttonLöschen4.Name = "buttonLöschen4";
            this.buttonLöschen4.Size = new System.Drawing.Size(56, 38);
            this.buttonLöschen4.TabIndex = 49;
            this.buttonLöschen4.Text = "Löschen";
            this.buttonLöschen4.UseVisualStyleBackColor = true;
            this.buttonLöschen4.Click += new System.EventHandler(this.buttonLöschen4_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 226);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(126, 13);
            this.label8.TabIndex = 45;
            this.label8.Text = "Löschen Zeile mit Spalte:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(149, 226);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 13);
            this.label9.TabIndex = 48;
            this.label9.Text = "wo der Text enthält:";
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(12, 243);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(133, 21);
            this.comboBox3.TabIndex = 46;
            // 
            // textBoxLöschen4
            // 
            this.textBoxLöschen4.Location = new System.Drawing.Point(149, 244);
            this.textBoxLöschen4.Name = "textBoxLöschen4";
            this.textBoxLöschen4.Size = new System.Drawing.Size(107, 20);
            this.textBoxLöschen4.TabIndex = 47;
            // 
            // numericUpDownBuchstaben
            // 
            this.numericUpDownBuchstaben.Location = new System.Drawing.Point(149, 196);
            this.numericUpDownBuchstaben.Name = "numericUpDownBuchstaben";
            this.numericUpDownBuchstaben.Size = new System.Drawing.Size(107, 20);
            this.numericUpDownBuchstaben.TabIndex = 43;
            // 
            // buttonLöschen3
            // 
            this.buttonLöschen3.Location = new System.Drawing.Point(262, 178);
            this.buttonLöschen3.Name = "buttonLöschen3";
            this.buttonLöschen3.Size = new System.Drawing.Size(56, 38);
            this.buttonLöschen3.TabIndex = 42;
            this.buttonLöschen3.Text = "Löschen";
            this.buttonLöschen3.UseVisualStyleBackColor = true;
            this.buttonLöschen3.Click += new System.EventHandler(this.buttonLöschen3_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 178);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 39;
            this.label5.Text = "Löschen Zeile:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(149, 178);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 13);
            this.label6.TabIndex = 41;
            this.label6.Text = "Buchstabenanzahl <=:";
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(12, 195);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(133, 21);
            this.comboBox2.TabIndex = 40;
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(149, 148);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(107, 20);
            this.numericUpDown2.TabIndex = 37;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(12, 148);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(133, 20);
            this.numericUpDown1.TabIndex = 36;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 35;
            this.label3.Text = "Lösche Zeile von:";
            // 
            // buttonLöschen2
            // 
            this.buttonLöschen2.Location = new System.Drawing.Point(262, 130);
            this.buttonLöschen2.Name = "buttonLöschen2";
            this.buttonLöschen2.Size = new System.Drawing.Size(56, 38);
            this.buttonLöschen2.TabIndex = 34;
            this.buttonLöschen2.Text = "Löschen";
            this.buttonLöschen2.UseVisualStyleBackColor = true;
            this.buttonLöschen2.Click += new System.EventHandler(this.buttonLöschen2_Click);
            // 
            // buttonLöschen1
            // 
            this.buttonLöschen1.Location = new System.Drawing.Point(262, 83);
            this.buttonLöschen1.Name = "buttonLöschen1";
            this.buttonLöschen1.Size = new System.Drawing.Size(56, 38);
            this.buttonLöschen1.TabIndex = 33;
            this.buttonLöschen1.Text = "Löschen";
            this.buttonLöschen1.UseVisualStyleBackColor = true;
            this.buttonLöschen1.Click += new System.EventHandler(this.buttonLöschen1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "Löschen Zeile:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(149, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "wo der Text lautet:";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(12, 97);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(133, 21);
            this.comboBox1.TabIndex = 30;
            // 
            // textBoxSuE
            // 
            this.textBoxSuE.Location = new System.Drawing.Point(149, 98);
            this.textBoxSuE.Name = "textBoxSuE";
            this.textBoxSuE.Size = new System.Drawing.Size(107, 20);
            this.textBoxSuE.TabIndex = 31;
            // 
            // buttonLöschen5
            // 
            this.buttonLöschen5.Location = new System.Drawing.Point(262, 274);
            this.buttonLöschen5.Name = "buttonLöschen5";
            this.buttonLöschen5.Size = new System.Drawing.Size(56, 38);
            this.buttonLöschen5.TabIndex = 50;
            this.buttonLöschen5.Text = "Löschen";
            this.buttonLöschen5.UseVisualStyleBackColor = true;
            this.buttonLöschen5.Click += new System.EventHandler(this.button11_Click);
            // 
            // labelEinträge
            // 
            this.labelEinträge.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelEinträge.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEinträge.Location = new System.Drawing.Point(9, 380);
            this.labelEinträge.Name = "labelEinträge";
            this.labelEinträge.Size = new System.Drawing.Size(309, 19);
            this.labelEinträge.TabIndex = 51;
            this.labelEinträge.Text = "0 Einträge";
            this.labelEinträge.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelEinträge.Click += new System.EventHandler(this.labelEinträge_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 274);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(133, 13);
            this.label7.TabIndex = 52;
            this.label7.Text = "Lösche doppelte in Spalte:";
            // 
            // comboBox4
            // 
            this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(12, 291);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(244, 21);
            this.comboBox4.TabIndex = 53;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(149, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 54;
            this.label4.Text = "Lösche Zeile bis:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(212, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 23);
            this.button1.TabIndex = 55;
            this.button1.Text = "Zusammenführung";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonLöschen6
            // 
            this.buttonLöschen6.Location = new System.Drawing.Point(262, 324);
            this.buttonLöschen6.Name = "buttonLöschen6";
            this.buttonLöschen6.Size = new System.Drawing.Size(56, 38);
            this.buttonLöschen6.TabIndex = 56;
            this.buttonLöschen6.Text = "Löschen";
            this.buttonLöschen6.UseVisualStyleBackColor = true;
            this.buttonLöschen6.Click += new System.EventHandler(this.button2_Click);
            // 
            // comboBox5
            // 
            this.comboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(12, 341);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(133, 21);
            this.comboBox5.TabIndex = 57;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 325);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(132, 13);
            this.label10.TabIndex = 58;
            this.label10.Text = "Löschen Zeilen mit Spalte:";
            // 
            // textBoxZeichen
            // 
            this.textBoxZeichen.Location = new System.Drawing.Point(149, 341);
            this.textBoxZeichen.Name = "textBoxZeichen";
            this.textBoxZeichen.Size = new System.Drawing.Size(84, 20);
            this.textBoxZeichen.TabIndex = 59;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(146, 325);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(116, 13);
            this.label11.TabIndex = 60;
            this.label11.Text = "Wo Zeichen enthalten:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(232, 340);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(24, 22);
            this.button2.TabIndex = 61;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1276, 524);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.textBoxZeichen);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.comboBox5);
            this.Controls.Add(this.buttonLöschen6);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.comboBox4);
            this.Controls.Add(this.labelEinträge);
            this.Controls.Add(this.buttonLöschen5);
            this.Controls.Add(this.buttonLöschen4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.textBoxLöschen4);
            this.Controls.Add(this.numericUpDownBuchstaben);
            this.Controls.Add(this.buttonLöschen3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.numericUpDown2);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonLöschen2);
            this.Controls.Add(this.buttonLöschen1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.textBoxSuE);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.buttonLeeren1);
            this.Controls.Add(this.buttonDateiÖffnen1);
            this.Controls.Add(this.buttonDateiSpeichern1);
            this.Name = "Form1";
            this.Text = "Visu Translation Tool";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBuchstaben)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonDateiÖffnen1;
        private System.Windows.Forms.Button buttonDateiSpeichern1;
        private System.Windows.Forms.Button buttonLeeren1;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonLöschen4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.TextBox textBoxLöschen4;
        private System.Windows.Forms.NumericUpDown numericUpDownBuchstaben;
        private System.Windows.Forms.Button buttonLöschen3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonLöschen2;
        private System.Windows.Forms.Button buttonLöschen1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBoxSuE;
        private System.Windows.Forms.Button buttonLöschen5;
        private System.Windows.Forms.Label labelEinträge;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonLöschen6;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxZeichen;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button2;
    }
}

