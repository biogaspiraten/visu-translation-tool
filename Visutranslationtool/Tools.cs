﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VisuTranslationTool
{
    public partial class Tools : Form
    {
        DataGridView dgv;

        public Tools(DataGridView dgv1)
        {
            InitializeComponent();
            dgv = dgv1;

            //combobox bestücken
            comboBox1.Items.Clear();
            foreach (DataGridViewColumn item in dgv.Columns)
            {
                comboBox1.Items.Add(item.HeaderText);
                comboBox2.Items.Add(item.HeaderText);
                comboBox3.Items.Add(item.HeaderText);
            }
        }


        private void delDittos()
        {
            //Liste sortieren, damit man die doppelten Einträge löschen kann.
            ListSortDirection sortDirection = ListSortDirection.Ascending;
            dgv.Sort(dgv.Columns[5], sortDirection);



            //Doppelte Einträge erkennen und den Index merken
            string strLast = null;
            List<int> listInt = new List<int>();
            foreach (DataGridViewRow row in dgv.Rows)
            {
                if (row.Cells[5].Value != null)
                {
                    if (strLast == row.Cells[5].Value.ToString())
                    {
                        listInt.Add(row.Index);
                    }
                    strLast = row.Cells[5].Value.ToString();
                }
            }

            //Liste der Indexe der zu löschenden Dateien sortieren
            listInt.Sort();

            //Einträge löschen
            for (int i = listInt.Count; i > 0; i--)
            {
                try
                {
                    DataGridViewRow row = dgv.Rows[listInt[i]];
                    dgv.Rows.Remove(row);
                }
                catch (Exception)
                {

                }
            }
        }

        private Point mouseposition;
        void MainFormMouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            mouseposition = new Point(-e.X, -e.Y);
        }

        void MainFormMouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouseposition.X, mouseposition.Y);
                Location = mousePos;
            }
        }

        private void Tools_Deactivate(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonLöschen2_Click(object sender, EventArgs e)
        {
            for (int i = Convert.ToInt32(numericUpDown2.Value); i > Convert.ToInt32(numericUpDown1.Value); i--)
            {
                try
                {
                    dgv.Rows.RemoveAt(i);
                }
                catch (Exception)
                {
                    
                    throw;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            delDittos();
        }

        private void buttonLöschen1_Click(object sender, EventArgs e)
        {
            int iIndex = 0;

            foreach (DataGridViewColumn column  in dgv.Columns)
            {
                if (column.HeaderText == comboBox1.Text)
                {
                    iIndex = column.Index;
                    break;
                }
            }

            for (int i = dgv.Rows.Count - 1; i > 0; i--)
            {
                try
                {
                    if ((string)dgv.Rows[i].Cells[iIndex].Value == textBoxSuE.Text)
                    {
                        dgv.Rows.Remove(dgv.Rows[i]);
                    }
                }
                catch (Exception)
                {

                }
            }
        }

        private void buttonLöschen3_Click(object sender, EventArgs e)
        {
            int iIndex = 0;

            foreach (DataGridViewColumn column in dgv.Columns)
            {
                if (column.HeaderText == comboBox2.Text)
                {
                    iIndex = column.Index;
                    break;
                }
            }

            List<string> listZeichen = new List<string>();
            for (int i = 65; i < 90; i++)
            {
                listZeichen.Add(Convert.ToChar(i).ToString());
            }
            for (int i = 97; i < 122; i++)
            {
                listZeichen.Add(Convert.ToChar(i).ToString());
            }

            for (int i1 = dgv.Rows.Count; i1 > 0; i1--)
            {
                try
                {
                    DataGridViewRow row = dgv.Rows[i1];

                    int iCounter = 0;
                    string strValue = row.Cells[iIndex].Value.ToString();

                    for (int i = 0; i < strValue.Length; i++)
                    {
                        foreach (string zeichen in listZeichen)
                        {
                            if (strValue.Substring(i, 1) == zeichen)
                            {
                                iCounter++;
                                break;
                            }
                        }
                    }

                    if (iCounter < Convert.ToInt32(numericUpDownBuchstaben.Value))
                    {
                        dgv.Rows.Remove(row);
                    }
                }
                catch (Exception)
                {

                }
            }
        }

        private void buttonLöschen4_Click(object sender, EventArgs e)
        {
            int iIndex = 0;

            foreach (DataGridViewColumn column in dgv.Columns)
            {
                if (column.HeaderText == comboBox3.Text)
                {
                    iIndex = column.Index;
                    break;
                }
            }

            for (int i = dgv.Rows.Count - 1; i > 0; i--)
            {
                try
                {
                    if (dgv.Rows[i].Cells[iIndex].Value.ToString().Contains(textBoxLöschen4.Text))
                    {
                        dgv.Rows.Remove(dgv.Rows[i]);
                    }
                }
                catch (Exception)
                {

                }
            }
        }
    }
}
